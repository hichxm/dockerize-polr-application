FROM ajanvier/polr

COPY --chown=nobody start.sh /start.sh
RUN chmod u+x /start.sh

ENTRYPOINT /wait-for-it.sh $DB_HOST:$DB_PORT --strict --timeout=120 -- /start.sh